import React from 'react';

function Stream({ user, tracks = [], onAuth }) {
  return (
    <div>
      <div>
        {
          user ?
            <div>{user.username}</div> :
            <button onClick={onAuth} type="button">Login</button>
        }
      </div>
      <br />
      <div>
        {
          tracks.map(track =>
            (<div className="track" key={track.origin.id}>{track.origin.title}</div>))
        }
      </div>
    </div>
  );
}

Stream.propTypes = {
  user: React.PropTypes.shape({
    username: React.PropTypes.string.isRequired
  }).isRequired,
  tracks: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      origin: React.PropTypes.shape({
        id: React.PropTypes.number.isRequired,
        title: React.PropTypes.string.isRequired
      })
    })).isRequired,
  onAuth: React.PropTypes.func.isRequired
};


export default Stream;
